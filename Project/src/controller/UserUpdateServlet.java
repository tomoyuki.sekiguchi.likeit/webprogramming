package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// TODO 未実装：ログインセッションがある場合、ユーザ一覧画面にリダイレクトさせる
				HttpSession session = request.getSession();
				User user2 = (User)session.getAttribute("userInfo");

				if(user2 == null) {
					response.sendRedirect("LoginServlet");
					return;

				}

	// URLからGETパラメータとしてIDを受け取る
			String id = request.getParameter("id");

			// 確認用：idをコンソールに出力
			System.out.println(id);


			// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
			UserDao userDao = new UserDao();
			User user = userDao.findById(id);

			request.setAttribute("userUpdate", user);

			// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);

	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");

		UserDao userDao = new UserDao();


		//エラー処理
		/**
		 * パスワードとパスワード確認が異なる場合
		 * パスワード以外にnullがある場合
		 * 「入力された情報は正しくありません」**/


		if (!password.equals(password2)) {

			// リクエストスコープにエラーメッセージをセット

						request.setAttribute("errMsg", "入力された内容は正しくありません");

						// ログインjspにフォワード
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
						dispatcher.forward(request, response);
						return;

		}else if(name.equals("") ||birthDate.equals("")) {
			// リクエストスコープにエラーメッセージをセット
						request.setAttribute("errMsg", "入力された内容は正しくありません");

						// ログインjspにフォワード
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
						dispatcher.forward(request, response);
						return;
		}else {
			/** 登録に成功した場合、ユーザ一覧画面に遷移する **/
			userDao.update(id,name,birthDate,password,password2);
			response.sendRedirect("UserListServlet");
		}

	}




}
