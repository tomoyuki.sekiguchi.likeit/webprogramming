package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserNewServlet
 */
@WebServlet("/UserNewServlet")
public class UserNewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserNewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO 未実装：ログインセッションがある場合、ユーザ一覧画面にリダイレクトさせる
				HttpSession session = request.getSession();
				User user = (User)session.getAttribute("userInfo");

				if(user == null) {
					response.sendRedirect("LoginServlet");
					return;

				}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userNew.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");

		UserDao userDao = new UserDao();


		/** 登録に失敗した場合は、入力画面に戻り、「入力された内容は正しくありません」と表示
		 * 1、すでに登録済みのログインIDが入力された場合
		 * 2、パスワードとパスワード(確認)が異なる場合
		 * 3、未入力の項目がある場合 **/



		UserDao userDao2 = new UserDao();
		User user = userDao2.findByLoginId(loginId);


		if (user != null) {

			// リクエストスコープにエラーメッセージをセット

						request.setAttribute("errMsg", "入力された内容は正しくありません");

						// ログインjspにフォワード
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userNew.jsp");
						dispatcher.forward(request, response);
						return;

		}else if(!password.equals(password2)) {
			// リクエストスコープにエラーメッセージをセット
						request.setAttribute("errMsg", "入力された内容は正しくありません");

						// ログインjspにフォワード
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userNew.jsp");
						dispatcher.forward(request, response);
						return;
		}else if(loginId.equals("") ||name.equals("") ||birthDate.equals("") ||password.equals("")) {
			//if else文に変更する？

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userNew.jsp");
			dispatcher.forward(request, response);
			return;
		}else {
		/** 登録に成功した場合、ユーザ一覧画面に遷移する **/
			userDao.registration(loginId,name,birthDate,password,password2);

			response.sendRedirect("UserListServlet");
		}

	}

}
