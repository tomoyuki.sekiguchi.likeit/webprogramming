<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ更新画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="userList.css" rel="stylesheet" type="text/css" />

<!-- BootstrapのCSS読み込み -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- オリジナルCSS読み込み -->
<link href="css/userDelete.css" rel="stylesheet">
<!-- Jqeryの読み込み -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
<!-- BootstrapのJS読み込み -->
	<script src="js/bootstrap.min.js">
</script>
<!-- レイアウトカスタマイズ用個別CSS -->


</head>

<body>

	<header>
		<nav class="navbar navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="userCreate.html"></a>
				</div>

				<ul class="nav navbar-nav navbar-right">
					<li class="navbar-text" style="color: white">${userInfo.name}
						さん</li>
					<li class="dropdown"><a href="LogoutServlet"
						class="navbar-link logout-link" style="color: red">ログアウト</a></li>
				</ul>
			</div>
		</nav>
	</header>

	<h3>ユーザ削除確認</h3>


	<div class="form-group row">
		<label for="inputID" class="col-sm-2 col-form-label">ログインID:${userDelete.loginId}</label>

	</div>
	<p>を本当に削除してよろしいでしょうか。</p>
	<div class="decision">
		<form action="UserListServlet" method="get">
			<button type="submit">キャンセル</button>
		</form>
		<form action="UserDeleteServlet" method="post">
		<input type="hidden" value=" ${userDelete.id}" name="id">
			<button type="submit" style="width: 120px;" name="ok">OK</button>
		</form>
	</div>


</body>

</html>