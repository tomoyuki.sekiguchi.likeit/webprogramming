<%@ page language="java" contentType = "text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
   <meta charset="UTF-8">
       <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ユーザ更新画面</title>
    <link rel="stylesheet"  	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"           integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="userList.css" rel="stylesheet" type="text/css" />

    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- オリジナルCSS読み込み -->
    <link href="css/userUpdate.css" rel="stylesheet">
    <!-- Jqeryの読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>
    <!-- レイアウトカスタマイズ用個別CSS -->


</head>

<body>

 <header>
      <nav class="navbar navbar-inverse">
      	<div class="container">
      		<div class="navbar-header">
            <a class="navbar-brand" href="userCreate.html"></a>
      		</div>

          <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text" style="color:white">${userInfo.name} さん </li>
  			<li class="dropdown">
  			  <a href="LogoutServlet" class="navbar-link logout-link" style="color:red">ログアウト</a>
            </li>
  		  </ul>
      	</div>
      </nav>
 </header>

    <h3>ユーザ情報更新</h3>

     <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

    <form action="UserUpdateServlet" method="post">
  <div class="form-group row">
    <label for="inputID" class="col-sm-2 col-form-label" >ログインID</label>
    <class="col-sm-10">
      ${userUpdate.loginId}
      <input type="hidden" value=" ${userUpdate.id}" name="id">
    </class>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">パスワード</label>
    <class="col-sm-10">
      <input type="password" class="form-control" id="inputPassword" name="password">
      </class>
  </div>
        <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">パスワード(確認)</label>
    <class="col-sm-10">
      <input type="password" class="form-control" id="inputPassword" name="password2">
      </class>
  </div>
        <div class="form-group row">
    <label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
    <class="col-sm-10">
      <input type="text" class="form-control" id="userName" name="name">
      </class>
  </div>
        <div class="form-group row">
    <label for="date" class="col-sm-2 col-form-label">生年月日</label>
    <class="col-sm-10">
      <input type="text" class="form-control" id="date" name="birthDate">
      </class>
  </div>
        <button type="submit">更新</button>
</form>

    <a href="UserListServlet"　style="color: blue">戻る</a>
</body>

</html>