<%@ page language="java" contentType = "text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
       <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ユーザ一覧画面</title>
    <link rel="stylesheet"  	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"           integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="userList.css" rel="stylesheet" type="text/css" />

    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- オリジナルCSS読み込み -->
    <link href="css/userList.css" rel="stylesheet">
    <!-- Jqeryの読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>
    <!-- レイアウトカスタマイズ用個別CSS -->


</head>

<body>
 <header>
      <nav class="navbar navbar-inverse">
      	<div class="container">
      		<div class="navbar-header">
            <a class="navbar-brand" href="userCreate.html"></a>
      		</div>

          <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text" style="color:white">${userInfo.name} さん </li>
  			<li class="dropdown">
  			  <a href="LogoutServlet" class="navbar-link logout-link" style="color:red">ログアウト</a>
            </li>
  		  </ul>
      	</div>
      </nav>
 </header>
    <h3>ユーザー一覧</h3>
    <div style="text-align: right">
    <a href="UserNewServlet" style="color: blue">新規登録</a>


    </div>

 <!-- 検索するためにaction="servlet" method="post" 処理をする？ -->

    <form action = "SearchServlet" method = "post">
  		<div class="form-group row">
      		<label for="inputID" class="col-sm-2 col-form-label">ログインID</label>
    		<class="col-sm-10">
      		<input type="text" class="form-control" id="inputID" name="loginId">
    		</class>
 		 </div>
  		<div class="form-group row">
    		<label for="inputUserName" class="col-sm-2 col-form-label">ユーザ名</label>
    		<class="col-sm-10">
		     <input type="text" class="form-control" id="inputUserName" name="name">
		     </class>
  		</div>
        <div class="form-group row">
            <label for="inputdate" class="col-sm-2 col-form-label">生年月日</label>
        <input name="date1" type="date" />　～　<input name="date2" type="date" />
        </div>
        <button type="submit" style="width:120px;">検索</button>
	</form>
    <hr>

    <!--footerかbody内にSQLを表示する(仮テーブルを作成する)  -->
        <div class="table-responsive">
             <table class="table table-striped">
               <thead>
                 <tr>
                   <th>ログインID</th>
                   <th>ユーザ名</th>
                   <th>生年月日</th>
                   <th></th>
                 </tr>
               </thead>
               <tbody>
                 <c:forEach var="user" items="${userList}" >


                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
            <!-- TODO 未実装；ログインボタンの表示制御を行う -->
                     <td>



                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>

                       <c:if test="${userInfo.loginId=='admin' or userInfo.loginId==user.loginId}">
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
					  </c:if>

					  <c:if test="${userInfo.loginId=='admin'}">
					  <!--  admin以外を表示する -->
                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
                       </c:if>



                     </td>
                   </tr>

                 </c:forEach>
               </tbody>
             </table>
           </div>

</body>

</html>