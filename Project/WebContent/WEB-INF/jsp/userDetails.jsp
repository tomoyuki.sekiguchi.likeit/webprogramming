<%@ page language="java" contentType = "text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
     <meta charset="UTF-8">
    <title>ユーザ詳細</title>
       <link rel="stylesheet"  	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"           integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <link href="css/userDetails.css" rel="stylesheet" >
      <!-- Jqeryの読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>
    <!-- レイアウトカスタマイズ用個別CSS -->

</head>

<body>

 <header>
      <nav class="navbar navbar-inverse">
      	<div class="container">
      		<div class="navbar-header">
            <a class="navbar-brand" href="userCreate.html"></a>
      		</div>

          <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text" style="color:white">${userInfo.name} さん </li>
  			<li class="dropdown">
  			  <a href="LogoutServlet" class="navbar-link logout-link" style="color:red">ログアウト</a>
            </li>
  		  </ul>
      	</div>
      </nav>
 </header>

    <h3>ユーザ情報詳細参照</h3>

    <form>
  <div class="form-group row">
    <label for="inputID" class="col-sm-2 col-form-label">ログインID</label>
    <class="col-sm-10">
      ${userDetail.loginId}
    </class>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">ユーザ名</label>
    <class="col-sm-10">
       ${userDetail.name}
      </class>
  </div>
        <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">生年月日</label>
    <class="col-sm-10">
        ${userDetail.birthDate}
      </class>
  </div>
        <div class="form-group row">
    <label for="userName" class="col-sm-2 col-form-label">登録日時</label>
    <class="col-sm-10">
       ${userDetail.createDate}
      </class>
  </div>
        <div class="form-group row">
    <label for="date" class="col-sm-2 col-form-label">更新日時</label>
    <class="col-sm-10">
       ${userDetail.updateDate}
      </class>
  </div>
</form>

    <a href="UserListServlet"　style="color: blue">戻る</a>
</body>

</html>